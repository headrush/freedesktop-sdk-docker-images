set -o errexit

dnf install -y gpg git
dnf install -y --enablerepo=updates-testing libabigail flatpak flatpak-builder
